import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import CKEditor from '@ckeditor/ckeditor5-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// import VueHead from 'vue-head'
import App from './App.vue'
import router from './router'
import store from './store'
// import '@/misc/register-service-worker'
// import '@/misc/handle-network-status'
// import '@/firebase/init'
// import '@/firebase/authentication'
// import '@/misc/handle-apple-install-prompt'
// import 'pwacompat'

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(CKEditor)
// Vue.use(VueHead)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

console.log(`
🍱 This app is made by Gourav Chatterjee  🍱

👉 https://beanstalkedu.com/

`)
