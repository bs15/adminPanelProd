import Vue from 'vue'
import Router from 'vue-router'
import Head from 'vue-head'
// import Home from '@/views/Home.vue'
import HelloWorld from '@/components/HelloWorld'
import CreatePage from '@/components/CreatePage'
import ViewPages from '@/components/ViewPages'
import SinglePage from '@/components/SinglePage'
import BlogHome from '@/components/BlogHome'
import FaqHome from '@/components/FaqHome'
import CreateFaq from '@/components/CreateFaq'
import ViewFaq from '@/components/ViewFaq'
import SingleFaq from '@/components/SingleFaq'

// import store from '@/store'

Vue.use(Router)

/* If you don't know about VueHead, please refer to https://github.com/ktquez/vue-head */

Vue.use(Head, {
  complement: process.env.VUE_APP_TITLE
})

/* If you don't know about VueRouter, please refer to https://router.vuejs.org/ */

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/createBlog',
      name: 'CreatePage',
      component: CreatePage
    },
    {
      path: '/viewBlog',
      name: 'ViewPages',
      component: ViewPages
    },
    {
      path: '/singlepage',
      name: 'SinglePage',
      component: SinglePage,
      props: true
    },
    {
      path: '/blogHome',
      name: 'BlogHome',
      component: BlogHome,
      props: true
    },
    {
      path: '/faqHome',
      name: 'FaqHome',
      component: FaqHome,
      props: true
    },
    {
      path: '/createFaq',
      name: 'CreateFaq',
      component: CreateFaq,
      props: true
    },
    {
      path: '/viewFaq',
      name: 'ViewFaq',
      component: ViewFaq,
      props: true
    },
    {
      path: '/singleFaq',
      name: 'SingleFaq',
      component: SingleFaq,
      props: true
    }
  ]
  //   {
  //     path: '/login',
  //     name: 'login',
  //     component: () =>
  //       import(/* webpackChunkName: "client-chunk-login" */ '@/views/Login.vue'),
  //     meta: {
  //       authNotRequired: true
  //     }
  //   },
  //   {
  //     path: '/products',
  //     name: 'products',
  //     component: () =>
  //       import(/* webpackChunkName: "client-chunk-products" */ '@/views/Products.vue')
  //   },
  //   {
  //     path: '/products/:id',
  //     name: 'product',
  //     props: true,
  //     component: () =>
  //       import(/* webpackChunkName: "client-chunk-product-details" */ '@/views/Product.vue')
  //   },
  //   { path: '*', redirect: '/home' }
  
})

/**
 * Handle user redirections
 */
// eslint-disable-next-line consistent-return
// router.beforeEach((to, from, next) => {
//   if (
//     !(to.meta && to.meta.authNotRequired) &&
//     isNil(store.state.authentication.user)
//   ) {
//     const path =
//       store.state.authentication.user === null ? '/login' : '/check-login'
//     return next(`${path}?redirectUrl=${to.path}`)
//   }
//   next()
// })

export default router
